#include <iostream>

int main() {

    int totalNumbers = 5;
    int numbers[totalNumbers];
    int maxNumber;

    numbers[0] = 21;
    numbers[1] = 44;
    numbers[2] = 98;
    numbers[3] = 4;
    numbers[4] = 7;

    for(int i = 0; i < totalNumbers; i++){
        if(i == 0)maxNumber=numbers[0];
        if(numbers[i] > maxNumber)maxNumber = numbers[i];
    }

    printf("El numero maximo es: %d", maxNumber);
    return 0;
}
