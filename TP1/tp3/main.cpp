#include <iostream>

int main() {

    int totalNumbers = 5;
    int numbers[totalNumbers];
    int minNumber;

    numbers[0] = 21;
    numbers[1] = 44;
    numbers[2] = 98;
    numbers[3] = 4;
    numbers[4] = -57;

    for(int i = 0; i < totalNumbers; i++){
        if(i == 0)minNumber=numbers[0];
        if(numbers[i] < minNumber)minNumber = numbers[i];
    }

    printf("El numero minimo es: %d", minNumber);
    return 0;
}
